local options = {
  cursorline     = false,
  mouse          = '',

  number         = true,
  relativenumber = true,

  shiftwidth     = 2,
  tabstop        = 2,
  expandtab      = true,
}

for key,val in pairs(options) do
  vim.opt[key] = val
end

