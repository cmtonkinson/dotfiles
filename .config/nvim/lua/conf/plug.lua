local Plug = vim.fn['plug#']

vim.call('plug#begin', '$HOME/.config/nvim/plugged')

Plug 'ThePrimeagen/vim-be-good'

Plug('junegunn/fzf', {['do'] = vim.fn['fzf#install']})
Plug 'junegunn/fzf.vim'

Plug 'neovim/nvim-lspconfig'
Plug 'RRethy/nvim-align'

Plug 'lewis6991/gitsigns.nvim'

Plug('catppuccin/nvim', { as = 'catppuccin'})

Plug 'github/copilot.vim'

vim.call('plug#end')

