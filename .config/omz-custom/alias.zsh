alias dotfiles='git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
alias list='ls -alsvh'
alias vim='nvim'
alias t="~/bin/t.zsh"
alias dc="docker-compose"
