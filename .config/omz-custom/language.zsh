##### Ruby
eval "$($HOME/.rbenv/bin/rbenv init - zsh)"

##### Python
export PYENV_ROOT="$HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

