# New system setup:

## 1. Install major dependencies
1. Install [zsh]
2. Install [oh-my-zsh]
3. Install [NeoVim]
4. Install [tpm]

Use `chsh` to set zsh as the default.

## 2. Configure dotfiles
```sh
alias dotfiles="git --git-dir=$HOME/.dotfiles --work-tree=$HOME"
dotfiles clone --bare git@gitlab.com:cmtonkinson/dotfiles.git $HOME/.dotfiles
dotfiles config --local status.showUntrackedFiles no
dotfiles checkout
```

## 3. Configure sybsystems
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

Install NeoVim plugins with `:PlugInstall`.

Install tmux plugins with `leader + I`.

Install `rbenv` and `ruby-build`:
```sh
git clone https://github.com/rbenv/rbenv.git $HOME/.rbenv
git clone https://github.com/rbenv/ruby-build.git $HOME/.rbenv/plugins/ruby-build
```

## 4. Restart terminal

## 5. Install Ruby
```sh
rbenv install --list
rbenv install <version>
rbenv global <version>
```


[zsh]: https://sourceforge.net/p/zsh/code/ci/master/tree/
[oh-my-zsh]: https://ohmyz.sh/#install
[NeoVim]: https://github.com/neovim/neovim/wiki/Installing-Neovim
[tpm]: https://github.com/tmux-plugins/tpm
