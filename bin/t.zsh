#!/bin/zsh

# This script automatically manages tmux sessions by using the current working directory (pwd) 
# and its parent directory to generate a unique session name. The session name will be formatted as:
#
#   <parent_directory_name>-<current_directory_name>
#
# When executed, the script will check if a tmux session with the generated name already exists:
#   - If a session exists, it will attach to that session.
#   - If no session exists, it will create a new tmux session with the generated name and attach to it.


# Get the current directory (pwd) and its parent directory
current_dir=$(basename "$PWD")
parent_dir=$(basename "$(dirname "$PWD")")

# Construct the session name by combining parent_dir and current_dir with a dash
session_name="${parent_dir}-${current_dir}"

# Check if a tmux session with the session name already exists
if tmux has-session -t "$session_name" 2>/dev/null; then
  # If the session exists, attach to it
  echo "Attaching to existing tmux session: $session_name"
  tmux attach-session -t "$session_name"
else
  # If the session does not exist, create it and attach
  echo "Creating new tmux session: $session_name"
  tmux new-session -s "$session_name"
fi

